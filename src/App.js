import React, { Fragment } from 'react'
import Packaging from './components/Packaging'
import './App.css';

const database = [

  {"type":"table","name":"component","database":"packaging-database","data":
    [
      {"id":"1","packaging_id":"1","name":"Barquette"},
      {"id":"2","packaging_id":"1","name":"Opercule"},
      {"id":"3","packaging_id":"1","name":"Cartonette"},
      {"id":"4","packaging_id":"2","name":"Conserve"},
      {"id":"5","packaging_id":"3","name":"Caisse"},
      {"id":"6","packaging_id":"3","name":"Couvercle"}
    ]
  },

  {"type":"table","name":"doctrine_migration_versions","database":"packaging-database","data":
    [
      {"version":"DoctrineMigrations\\Version20210112161019","executed_at":"2021-01-12 17:10:39","execution_time":"266"}
    ]
  },

  {"type":"table","name":"material","database":"packaging-database","data":
    [
      {"id":"1","component_id":"1","name":"Polypropylène","matter":"Polypropylène","mass":"150","volume":"100"},
      {"id":"2","component_id":"2","name":"Polychlorure de vinyle","matter":"Polychlorure de vinyle","mass":"20","volume":"0"},
      {"id":"3","component_id":"3","name":"Carton plat","matter":"Carton plat","mass":"10","volume":"0"},
      {"id":"4","component_id":"4","name":"Acier","matter":"Acier","mass":"100","volume":"200"},
      {"id":"5","component_id":"5","name":"Bois peint","matter":"Bois peint","mass":"500","volume":"2000"},
      {"id":"6","component_id":"6","name":"Bois brut","matter":"Bois brut","mass":"100","volume":"0"},
      {"id":"7","component_id":"6","name":"Polypropylène","matter":"Polypropylène","mass":"10","volume":"0"}
    ]
  },

  {"type":"table","name":"packaging","database":"packaging-database","data":
    [
      {"id":"1","name":"Barquette PP"},
      {"id":"2","name":"Conserve acier"},
      {"id":"3","name":"Caisse bois"}
    ]
  }
]

class App extends React.Component {

  state = {
    database: database
  }

  render(){

    return (
      <Fragment>
        <div className="App">
          <h1>Packagings</h1>
          { this.state.database.filter( databaseTable => databaseTable['name'] === 'packaging')[0]['data'].map( (packaging) =>
            <Packaging key={packaging['id']} packagingId={packaging['id']} name={packaging['name']} database={database} ></Packaging>
          ) }
        </div>
      </Fragment>
    )
  }

}

export default App;
