import React, { Fragment } from 'react'
import './Packaging.css';
import Component from './Component'

class Packaging extends React.Component {

  state = {
    name: this.props.name,
    isShow: false
  }

  handleShowComponents = () => {
    const isShow = !this.state.isShow
    this.setState( { isShow } )
  }


  render(){

    const { name, isShow } = this.state

    return(
      <Fragment>
        <div className='packaging-container'>
          <Fragment>
            <h1>{name}</h1>

            <button onClick={this.handleShowComponents}>{ isShow ? 'Close panel' : 'Modify packaging' }</button>

            {
              isShow ?
              <Fragment>
                { this.props.database.filter( databaseTable => databaseTable['name'] === 'component' )[0]['data']
                  .filter( component => component['packaging_id'] === this.props.packagingId).map( component =>
                    <Component key={component['id']}
                               componentId={component['id']}
                               packagingId={this.props.packagingId} 
                               name={component['name']}
                               database={this.props.database} />
                ) }
              </Fragment> : null
            }


          </Fragment>
        </div>
      </Fragment>
    )
  }
}

export default Packaging
