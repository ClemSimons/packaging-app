import React, { Fragment } from 'react'
import './Component.css';
import Material from './Material'

class Component extends React.Component {

  state = {
    name: this.props.name
  }

  handleChange = event => {
    const newName = event.target.value
    this.setState( { name: newName } )
  }


  render(){
    return(
      <Fragment>
        <div className='component-container'>

          <h2>{this.state.name}</h2>

          <p>Change component name: </p><span><input onChange={this.handleChange}></input></span>

          { this.props.database.filter( databaseTable => databaseTable['name'] === 'material' )[0]['data']
            .filter( material => material['component_id'] === this.props.componentId).map( (material) =>
            <Material key={material['id']}
                      name={material['name']}
                      matter={material['matter']}
                      mass={material['mass']}
                      volume={material['volume']}
                      materialId={material['id']}
                      componentId={this.props.componentId}
                      packagingId={this.props.packagingId} ></Material>
          ) }

        </div>
      </Fragment>
    )
  }
}

export default Component
