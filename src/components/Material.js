import React, { Fragment } from 'react'
import './Material.css';

class Material extends React.Component {

  state = {
    name: this.props.name,
    matter: this.props.matter,
    mass: this.props.mass,
    volume: this.props.volume
  }

  handleSubmit = (value, attribute) => {
    const newValue = value
    const entries = new Map([ new Array(attribute, newValue) ])
    const newObject = Object.fromEntries(entries)
    this.setState( newObject )
  }


  render(){
    return(
      <Fragment>
        <div className='material-container'>
          {
            Object.entries(this.state).map( (arrayContainingMaterialAttributeAndValue) => (
              <Fragment key={arrayContainingMaterialAttributeAndValue[0] + arrayContainingMaterialAttributeAndValue[1]}>
                <Fragment>
                  <h3>{arrayContainingMaterialAttributeAndValue[0] + ' : ' + arrayContainingMaterialAttributeAndValue[1]}</h3>
                </Fragment>
                <Fragment>

                  <form onSubmit={() =>
                      this.handleSubmit((document.getElementById(this.props.materialId + arrayContainingMaterialAttributeAndValue[0])
                      .value), arrayContainingMaterialAttributeAndValue[0])}>

                    <p>Change material {arrayContainingMaterialAttributeAndValue[0]}: </p>

                    <input id={this.props.materialId + arrayContainingMaterialAttributeAndValue[0]} />

                    <button type='submit'>Submit</button>
                  </form>
                </Fragment>
              </Fragment>
            ) )
          }
        </div>
      </Fragment>
    )
  }
}

export default Material
